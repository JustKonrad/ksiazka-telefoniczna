from django.db import models


class Person(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    class Meta:
        ordering = ('last_name', )

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def no_emails_or_numbers(self):
        if not self.email_set.all() and not self.phone_set.all():
            return True
        return False


class Phone(models.Model):
    phone = models.CharField(max_length=50)
    person = models.ForeignKey(Person,
                               on_delete=models.CASCADE, editable=False)


class Email(models.Model):
    email = models.EmailField()
    person = models.ForeignKey(Person,
                               on_delete=models.CASCADE, editable=False)
