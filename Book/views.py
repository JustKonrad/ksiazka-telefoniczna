from django.views.generic import (ListView,
                                  CreateView,
                                  DeleteView,
                                  UpdateView)
from django.db.models import Q
from django.shortcuts import reverse, get_object_or_404
from .models import Person, Phone, Email
from .forms import PersonForm, PhoneForm, EmailForm


class PeopleListView(ListView):
    context_object_name = 'people'
    template_name = 'person/people_list.html'
    paginate_by = 5

    def get_queryset(self):
        """get_queryset method overridden for
        implementing basic search engine"""
        query = self.request.GET.get('query')
        category = self.request.GET.get('category')
        if category == 'first_name':
            people = Person.objects.filter(Q(first_name__icontains=query))
        elif category == 'last_name':
            people = Person.objects.filter(Q(last_name__icontains=query))
        elif category == 'phone':
            people = Person.objects.filter(Q(phone__phone__contains=query))
        elif category == 'email':
            people = Person.objects.filter(Q(email__email__contains=query))
        else:
            people = Person.objects.all()
        return people


class CreatePersonView(CreateView):
    form_class = PersonForm
    model = Person
    template_name = 'person/add_person.html'

    def get_success_url(self):
        return reverse('person:people_list')


class DeletePersonView(DeleteView):
    model = Person
    context_object_name = 'person'
    template_name = 'person/delete_person.html'

    def get_object(self, **kwargs):
        person_id = self.kwargs.get('id')
        return get_object_or_404(Person, id=person_id)

    def get_success_url(self):
        return reverse('person:people_list')


class UpdatePersonView(UpdateView):
    model = Person
    form_class = PersonForm
    template_name = 'person/update_person.html'

    def get_object(self, **kwargs):
        person_id = self.kwargs.get('id')
        return get_object_or_404(Person, id=person_id)

    def get_success_url(self):
        return reverse('person:people_list')


class AddPhoneNumberView(CreateView):
    model = Phone
    form_class = PhoneForm
    template_name = 'person/add_phone.html'

    def form_valid(self, form):
        phone = form.save(commit=False)
        phone.person_id = self.kwargs.get('id')
        return super(AddPhoneNumberView, self).form_valid(form)

    def get_success_url(self):
        return reverse('person:people_list')


class AddEmailView(CreateView):
    model = Email
    form_class = EmailForm
    template_name = 'person/add_email.html'

    def form_valid(self, form):
        email = form.save(commit=False)
        email.person_id = self.kwargs.get('id')
        return super(AddEmailView, self).form_valid(form)

    def get_success_url(self):
        return reverse('person:people_list')
