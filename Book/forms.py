import re
from django.forms import ModelForm, ValidationError

from .models import Person, Phone, Email


class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = ['first_name', 'last_name']
        labels = {
            'first_name': 'Imie',
            'last_name': 'Nazwisko',
        }


class PhoneForm(ModelForm):
    class Meta:
        model = Phone
        fields = ['phone']
        labels = {'phone': 'Numer telefonu'}

    def clean_phone(self):
        pattern = re.compile(r'^\d{3}-\d{3}-\d{3}$|^\d{2}-\d{3}-\d{2}-\d{2}$')
        phone = self.cleaned_data['phone']
        if not pattern.fullmatch(phone):
            raise ValidationError('Niewłaściwy numer telefonu')
        return phone


class EmailForm(ModelForm):
    class Meta:
        model = Email
        fields = ['email']
        labels = {'email': 'Adres email'}

    def clean_email(self):
        pattern = re.compile(r'^(?!\d).*[a-zA-Z0-9]*@(\w*.\w*)|(\w*.)|.(\w*)$')
        email = self.cleaned_data['email']
        if not pattern.fullmatch(email):
            raise ValidationError('Niewłaściwy adres e-mail')
        return email
