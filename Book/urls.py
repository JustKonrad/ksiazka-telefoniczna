from django.urls import path
from .views import (PeopleListView,
                    CreatePersonView,
                    DeletePersonView,
                    UpdatePersonView,
                    AddPhoneNumberView,
                    AddEmailView)

app_name = 'person'
urlpatterns = [
    path('', PeopleListView.as_view(), name='people_list'),
    path('add/', CreatePersonView.as_view(), name='create_person'),
    path('delete/<int:id>', DeletePersonView.as_view(), name='delete_person'),
    path('update/<int:id>', UpdatePersonView.as_view(), name='update_person'),
    path('add-number/<int:id>', AddPhoneNumberView.as_view(), name='add_number'),
    path('add-email/<int:id>', AddEmailView.as_view(), name='add_email')
]
